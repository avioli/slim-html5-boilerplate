/* TEAM */
      
Name: Beth Lawler
Role: Front-End Development
Location: Hobart, Australia.
Developed for: Ionata Web Solutions. (http://ionata.com.au)
              
/* SITE */
                            
Standards: HTML5 + CSS3 
Components: Modernizr, Selectivizr & jQuery   
